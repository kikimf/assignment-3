package router

import (
	"assignment3/controllers"

	"github.com/gin-gonic/gin"
)

func StartApp() *gin.Engine {
	r := gin.Default()
	r.LoadHTMLGlob("templates/*.html")
	userRouter := r.Group("/users")
	{
		userRouter.GET("/test", controllers.Test)
	}

	return r
}
