package models

type Status struct {
	Status Element `json:"status"`
}

type Element struct {
	Water int `json:"water"`
	Wind  int `json:"wind"`
}
