package controllers

import (
	"assignment3/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Test(c *gin.Context) {

	status, err := utils.ReadJsonFile()
	if err != nil {
		c.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"error": err.Error(),
		})
	} else {
		water, wind := utils.ResponseGetData(status)
		c.HTML(http.StatusOK, "index.html", gin.H{
			"waterStatus": water,
			"windStatus":  wind,
			"wind":        status.Status.Wind,
			"water":       status.Status.Water,
		})
	}
}
