package main

import (
	"assignment3/router"
	"assignment3/utils"
	"fmt"
	"time"

	"github.com/go-co-op/gocron"
)

func main() {
	go func() {
		fmt.Println("test")
		s1 := gocron.NewScheduler(time.UTC)

		s1.Every(15).Seconds().Do(utils.UpdateJsonFile)
		s1.StartAsync()

	}()
	r := router.StartApp()
	r.Run(":8000")
}
