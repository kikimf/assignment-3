package utils

import "assignment3/models"

func ResponseGetData(status models.Status) (string, string) {
	var wind string
	var water string
	if status.Status.Water < 5 {
		water = "aman"
	} else if status.Status.Water > 6 && status.Status.Water < 8 {
		water = "siaga"
	} else {
		water = "bahaya"
	}

	if status.Status.Wind < 6 {
		wind = "aman"
	} else if status.Status.Wind > 7 && status.Status.Wind < 15 {
		wind = "siaga"
	} else {
		wind = "bahaya"
	}

	return water, wind
}
