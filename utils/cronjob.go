package utils

import (
	"assignment3/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"time"
)

func UpdateJsonFile() {
	fmt.Println("Run Function")
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	wind := r1.Intn(20)
	water := r1.Intn(10)

	status := models.Status{}
	status.Status.Water = water
	status.Status.Wind = wind

	content, err := json.Marshal(status)
	if err != nil {
		fmt.Println(err)
	}
	err = ioutil.WriteFile("utils/data.json", content, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func ReadJsonFile() (models.Status, error) {
	content, err := ioutil.ReadFile("utils/data.json")

	if err != nil {
		fmt.Println(err)
		return models.Status{}, err
	}

	status := models.Status{}
	err = json.Unmarshal(content, &status)

	if err != nil {
		fmt.Println(err)
		return models.Status{}, err
	}
	return status, nil
}
